# Various-Places-on-Canvas-Java
import javax.microedition.lcdui*;

public class TextCanvas
	extends Canvas {
     public void paint (Graphics g) {
	     int w = getWidth();
	     int h = getHeight();
	     
	     g.setColor(0xffffff);
	     g.fillRect(0, 0, w, h);
	     g.setColor(0x00000);
	     
	     
	     // First label the four corners.
	     g.drawingString("corner", 0, 0,
	          Graphics.TOP | Graphics.LEFT);
	     g.drawString("corner", w, 0,
	          Graphics.TOP | Graphics.RIGHT);
	     g.drawString("corner", w, h,
	            Graphics.BOTTOM | Graphics.RIGHT);
	     
	// Now put something in the middle (more or less).
	    g.drawingString("Sin Wagon", w / 2, h /  2,
	             Graphics.BASELINE | Graphics.RIGHT);

// Now put something in the middle (more or less).
   g.drawing("Sin Wagon" , w / 2, h / 2,
	    Graphics.BASELINE | Graphics.HCENTER);
  }
}
